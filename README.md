# YoYoButton Reward Wallet (PhoneGap 3.x and older)

## Which PhoneGap?
These files are created and test in PhoneGap 3 but it should be fine for all versions as they are just js, html and css.

## Dependencies
You need to have have:

- [jQuery library](http://jquery.com/) which is already incldued in the files.
- [jQuery GUID](https://code.google.com/p/accept/source/browse/html/res/jquery.Guid.js?r=3e72134c6a29a9ccf30f3a2d0802bca7d14788ea) which is already incldued in the files.

## Get Started
1. Copy the files provided in this repository to **www** folder.
2. If you change the folder instruction you should update references in **yoyo.html** file.
3. In your main html file where you want to open a wallet (maybe by clicking a button) add **yoyo.js** to head tag of the html file in between jQuery and your main js file:

 `<script type="text/javascript" src="js/yoyo.js"></script>`

4. Open the **yoyo.js** file and replace the **api** in *yoyo.app.api*. You should use the API you've got from YoYoButton Sandbox (Don't have one? Get it from https://sandbox.yoyobutton.com).
5. And on whatever action you want you can open wallet by calling **yoyo.wallet.load()** method.
6. Running your App, on clicking the button which is opening the wallet "button which calling yoyo.wallet.load", either you see your wallet or you get redirected to **yoyo.html** page automatically.
7. Later you can customise **yoyo.html** or totally get rid of it if you have the user email in your app already.
8. Enjoy your wallet!

## Before Going Live
* Make sure you have changed the **live** attribute to *true* in yoyo.app in **yoyo.js** file.
* Make sure you have done all steps listed in YoYo sandbox Tutorial/Simulator Page at **"Before Going Live"** Section.

## Customisation
* All the variables and methods can be overridden but most likely the only parameters/variables you need to change are defined within **yoyo.app**
* **api:** Api provided by YoYo
* **live:** in case you are in production and going live it should be true. Default is *false* and force the system to work with sandbox.
* **email_storage_name:** Variable name which stores the email in **window.localStorage**. Default is *'yoyo.email'*
* **token_storage_name:** Variable name which stores the security token in **window.localStorage**. Default is *'yoyo.token'*
* **email_save_field:** CSS selector to get email input. Default is *'.fn-yoyo-email-save-field'*
* **email_save_button:** CSS selector to get email save button. Default is *'.fn-yoyo-email-save'*
* **email_update_field:** CSS selector to get email input. Default is *'.fn-yoyo-email-update-field'*
* **email_update_button:** CSS selector to get email update button. Default is *'.fn-yoyo-email-update'*
* **pages:**
 * **home:** Home page of your application. Default is *'index.html'*.
 * **email:** First page to get the email before opening a wallet. Default is *'yoyo.html'*.
 * **change:** Update email page. Default is *'yoyo.html#yoyoupdate'*.

**If your application doesn't have store emails, you don't need to change anything except replacing the api parameters. This library will handle everything for you.**

**In case you already have email you may need to set it by calling below method before calling yoyo.wallet.load():**

```javascript
yoyo.email.set(THE_EMAIL_ADDRESS_FROM_YOUR_APP)
yoyo.wallet.load()
```

### Change Security Token Generation
We use jQuery.Guid plugin but you are free to use any system you like to. You can do it by replacing **yoyo.security.get()** method.

**This token should be created at app installation time and not changed after installation.**
