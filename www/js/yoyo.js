/*
  Replace api (yoyo.app.api) attribute with your api and live (yoyo.app.live) attribute with true before going live
*/
if(typeof yoyo == 'undefined') var yoyo = {};

$(function(){

$.extend( yoyo, {
  app: {
    api:                  'cc67de5b-c46a-43ce-aa47-3bc52f7711dc',
    live:                 false,
    email_storage_name:   'yoyo.email',
    token_storage_name:   'yoyo.token',
    email_save_field:     '.fn-yoyo-email-save-field',
    email_save_button:    '.fn-yoyo-email-save',
    email_update_field:   '.fn-yoyo-email-update-field',
    email_update_button:  '.fn-yoyo-email-update',
    actions: {
      close:              function(){ yoyo.wallet.close() },
      change_email:       function(){ yoyo.email.change() },
      settings:           function(){ yoyo.email.change() },
      wallet:             $.noop
    },
    pages: {
      home:               'index.html',
      email:              'yoyo.html',
      change:             'yoyo.html#yoyoupdate'
    },
    uuid: function(){
      return $.Guid.Value();
    }
  },
  init: function(){
    if(location.hash=='#yoyoupdate'){
      $('.fn-yoyo-start').hide();
      $('.fn-yoyo-change').show();
    }
    $(yoyo.app.email_save_button).click(yoyo.email.save);
    $(yoyo.app.email_update_button).click(yoyo.email.update);
  },
  system: {
    sandbox: 'http://sandbox.yoyobutton.com',
    prd: 'https://mobile.yoyobutton.com',
    url: function(){
      return (yoyo.app.live ? yoyo.system.prd : yoyo.system.sandbox);
    },
    urlWithParams: function(path){
      return yoyo.system.url()+path+'?api='+yoyo.app.api+'&security='+yoyo.security.get()+'&email='+yoyo.email.get();
    }
  },
  storage: window.localStorage,
  email: {
    get: function(){
      return yoyo.storage.getItem(yoyo.app.email_storage_name);
    },
    set: function(email){
      yoyo.storage.setItem(yoyo.app.email_storage_name, email);
    },
    save: function(){
      var email = $(yoyo.app.email_save_field).val();
      if(email!='')
        yoyo.email.set(email);
      yoyo.wallet.load();
    },
    change: function(){
      location.href = yoyo.app.pages.change;
      yoyo.wallet.window.close();
    },
    update: function(){
      $.post( yoyo.system.urlWithParams('/mobile/customer/update_email.json'), { new_email: $(yoyo.app.email_update_field).val() }, yoyo.email.updated );
    },
    updated: function(d){
      if(d.success)
        yoyo.wallet.load();
    }
  },
  security: {
    get: function(){
      if(yoyo.storage.getItem(yoyo.app.token_storage_name)==null)
        yoyo.storage.setItem(yoyo.app.token_storage_name, yoyo.app.uuid());
      return yoyo.storage.getItem(yoyo.app.token_storage_name);
    }
  },
  wallet: {
    window: null,
    isOk: function(){
      if(yoyo.app.api==null){
        alert('API is not stored, you can set it by setting yoyo.api!');
        return false;
      }
      if(yoyo.email.get()==null){
        location.href = yoyo.app.pages.email+'?api='+yoyo.app.api;
        return false;
      }
      return true;
    },
    load: function(){
      if(!yoyo.wallet.isOk()) return;
      yoyo.wallet.window = window.open(yoyo.system.urlWithParams('/mobile/wallet'), '_blank', 'location=no');
      yoyo.wallet.window.addEventListener('loadstart', yoyo.wallet.change );
    },
    change: function(e){
      var action = e.url.split('?')[0].split('/').pop();
      if(typeof yoyo.app.actions[action]=='undefined'){
        if(!yoyo.app.live)
          alert('You should define '+action+' method in "yoyo.app.actions"!');
        return;
      }
      yoyo.app.actions[action].call();
    },
    close: function(){
      yoyo.wallet.window.close();
    }
  }
});

yoyo.init();

});
